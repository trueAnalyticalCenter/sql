# SQL-скрипты для MySql базы данных #

## Дамп БД ##
* dump.sql

## Структура БД ##
### Keywords ###
|Имя			|Тип 			|
|---------------|---------------|
|ID				|int(11)		|
|Name			|varchar(2048)	|
|PersonID		|int(11)		|

### Pages ###
|Имя			|Тип 			|
|---------------|---------------|
|ID				|int(11)		|
|URL			|varchar(2048)	|
|SiteID			|int(11)		|
|FoundDateTime	|datetime		|
|LastScanDate	|datetime		|

### PersonPageRank ###
|Имя			|Тип 			|
|---------------|---------------|
|ID				|int(11)		|
|PersonID		|int(11)		|
|PageID			|int(11)		|
|Rank			|int(11)		|

### Persons ###
|Имя			|Тип 			|
|---------------|---------------|
|ID				|int(11)		|
|Name			|varchar(2048)	|
|AddedByAdminId	|int(11)		|

### Sites ###
|Имя			|Тип 			|
|---------------|---------------|
|ID				|int(11)		|
|Name			|varchar(2048)	|
|AddedByAdminId	|int(11)		|

### Users ###
|Имя			|Тип 			|
|---------------|---------------|
|ID				|int(11)		|
|Username		|varchar(512)	|
|Password		|char(32)		|
|isAdmin		|tinyint(1)		|
|CreateByAdminID|int(11)		|


