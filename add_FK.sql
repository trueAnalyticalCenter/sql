SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
-- -----------------------------------------------------
-- Table  `pages`
-- -----------------------------------------------------
create index `fk_personpagerank_pages` on `pages` (`ID`);
alter TABLE `personpagerank` ADD CONSTRAINT `fk_personpagerank_pages`
    FOREIGN KEY (`PageID`) REFERENCES  `pages` (`ID`);
-- -----------------------------------------------------
-- Table  `persons`
-- -----------------------------------------------------
create index `fk_personpagerank_persons` on `persons` (`ID`);
alter TABLE `personpagerank` add CONSTRAINT `fk_personpagerank_persons`
    FOREIGN KEY (`PersonID`) REFERENCES  `persons` (`ID`);
    
create index `fk_keywords_persons` on  `persons` (`ID`);
alter TABLE  `keywords` add CONSTRAINT `fk_keywords_persons`
    FOREIGN KEY (`PersonID`) REFERENCES  `persons` (`ID`);
-- Table `sites`
-- -----------------------------------------------------
create index `fk_pages_sites` on `sites` (`ID`);
ALTER TABLE  `pages` add CONSTRAINT `fk_pages_sites`
    FOREIGN KEY (`SiteID`) REFERENCES  `sites` (`ID`);
-- Table  `users`
-- -----------------------------------------------------
create index `fk_persons_users` on `users` (`ID`);
alter TABLE  `persons` ADD CONSTRAINT `fk_persons_users`
    FOREIGN KEY (`AddedByAdminId`) REFERENCES  `users` (`ID`);
    
create index `fk_sites_users` on `users` (`ID`);
alter TABLE  `sites` ADD CONSTRAINT `fk_sites_users`
    FOREIGN KEY (`AddedByAdminID`) REFERENCES `users` (`ID`);
