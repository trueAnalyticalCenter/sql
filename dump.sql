-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:8889
-- Время создания: Сен 29 2016 г., 16:03
-- Версия сервера: 5.5.38
-- Версия PHP: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `TrueStat`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Keywords`
--

DROP TABLE IF EXISTS `Keywords`;
CREATE TABLE IF NOT EXISTS `Keywords` (
`ID` int(11) NOT NULL,
  `Name` varchar(2048) NOT NULL,
  `PersonID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Pages`
--

DROP TABLE IF EXISTS `Pages`;
CREATE TABLE IF NOT EXISTS `Pages` (
`ID` int(11) NOT NULL,
  `URL` varchar(2048) NOT NULL,
  `SiteID` int(11) NOT NULL,
  `FoundDateTime` datetime NOT NULL,
  `LastScanDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `PersonPageRank`
--

DROP TABLE IF EXISTS `PersonPageRank`;
CREATE TABLE IF NOT EXISTS `PersonPageRank` (
  `ID` int(11) NOT NULL,
  `PersonID` int(11) NOT NULL,
  `PageID` int(11) NOT NULL,
  `Rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Persons`
--

DROP TABLE IF EXISTS `Persons`;
CREATE TABLE IF NOT EXISTS `Persons` (
`ID` int(11) NOT NULL,
  `Name` varchar(2048) NOT NULL,
  `AddedByAdminId` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Sites`
--

DROP TABLE IF EXISTS `Sites`;
CREATE TABLE IF NOT EXISTS `Sites` (
`ID` int(11) NOT NULL,
  `Name` varchar(256) NOT NULL,
  `AddedByAdminID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

DROP TABLE IF EXISTS `Users`;
CREATE TABLE IF NOT EXISTS `Users` (
`ID` int(11) NOT NULL,
  `Username` varchar(512) NOT NULL,
  `Password` char(32) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `createdByAdminId` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Keywords`
--
ALTER TABLE `Keywords`
 ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Pages`
--
ALTER TABLE `Pages`
 ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `PersonPageRank`
--
ALTER TABLE `PersonPageRank`
 ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Persons`
--
ALTER TABLE `Persons`
 ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Sites`
--
ALTER TABLE `Sites`
 ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Users`
--
ALTER TABLE `Users`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Keywords`
--
ALTER TABLE `Keywords`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Pages`
--
ALTER TABLE `Pages`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `PersonPageRank`
--
ALTER TABLE `PersonPageRank`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Persons`
--
ALTER TABLE `Persons`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Sites`
--
ALTER TABLE `Sites`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `Users`
--
ALTER TABLE `Users`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
