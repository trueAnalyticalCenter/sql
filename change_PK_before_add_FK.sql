alter table `keywords` drop primary key, add primary key(`ID`, `PresonID`);
alter table `pages` drop primary key, add primary key(`ID`, `SiteID`);
alter table `personpagerank` drop primary key, add primary key(`ID`, `PersonID`, `PageID`);
alter table `persons` drop primary key, add primary key(`ID`, `AddedByAdminId`);
alter table `sites` drop primary key, add primary key(`ID`, `AddedByAdminId`);
alter table `users` drop primary key, add primary key(`ID`, `createdByAdminId`);

